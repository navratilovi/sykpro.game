using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using SYKPro.Core;
using SYKPro_Game;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment()) {
    app.UseExceptionHandler("/Error");
}

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

Player.LoadActualPlayer();
// Add games here after their import by NuGet. Address the game by its name, i.e.,
//Games.AddGame(new SYKPro.Chemistry.Solutions.Chemistry_Solutions());

app.Run();
